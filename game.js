// import Velocity from './js/chapter05/01-velocity-1.js'
// new Velocity().drawFrame()

// import Velocity from './js/chapter05/02-velocity-2.js'
// new Velocity().drawFrame()

// import Velocity from './js/chapter05/03-velocity-angle-3.js'
// new Velocity().drawFrame()

// import FollowMouse from './js/chapter05/04-follow-mouse.js'
// new FollowMouse().drawFrame()

import RotationalVelocity from './js/chapter05/05-rotational-velocity.js'
new RotationalVelocity().drawFrame()

// ------------------------Chapter 04---------------------------
// import DrawingApp from './js/chapter04/01-drawing-app.js'
// new DrawingApp()

// import DrawingCurves from './js/chapter04/02-drawing-curves.js'
// new DrawingCurves().drawFrame()

// import CurveThroughPoint from './js/chapter04/03-curve-through-point.js'
// new CurveThroughPoint().drawFrame()

// import MultiCurve from './js/chapter04/04-multi-curve-1.js'
// new MultiCurve().drawFrame()

// import MultiCurve from './js/chapter04/05-multi-curve-2.js'
// new MultiCurve().drawFrame()

// import MultiCurve from './js/chapter04/06-multi-curve-3.js'
// new MultiCurve().drawFrame()

// import GradientFill from './js/chapter04/07-gradient-fill-1.js'
// new GradientFill().drawFrame()

// import GradientFill from './js/chapter04/08-gradient-fill-2.js'
// new GradientFill().drawFrame()

// import GradientFill from './js/chapter04/09-gradient-fill-radial.js'
// new GradientFill().drawFrame()

// import LoadImage from './js/chapter04/10-load-image.js'
// new LoadImage().drawFrame()

// import VideoFrame from './js/chapter04/12-video-frames.js'
// new VideoFrame().drawFrame()

// import SprayPaint from './js/chapter04/16-spray-paint.js'
// new SprayPaint()

// ------------------------Chapter 03---------------------------

// import RotateToMouse from './js/chapter03/01-rotate-to-mouse.js'
// new RotateToMouse().drawFrame()

// import Bobbing from './js/chapter03/02-bobbing-1.js'
// new Bobbing().drawFrame()

// import Wave from './js/chapter03/04-wave-1.js'
// new Wave().drawFrame()

// import Pulse from './js/chapter03/05-pulse.js'
// new Pulse().drawFrame()

// import Random from './js/chapter03/06-random.js'
// new Random().drawFrame()

// import Wave from './js/chapter03/07-wave-2.js'
// new Wave().drawFrame() 

// import Circle from './js/chapter03/08-circle.js'
// new Circle().drawFrame()

// import Oval from './js/chapter03/09-oval.js'
// new Oval().drawFrame()

// import Distance from './js/chapter03/10-distance'
// new Distance().drawFrame()

