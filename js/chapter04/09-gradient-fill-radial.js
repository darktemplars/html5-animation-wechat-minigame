import Ball from '../sprite/ball.js'

export default class GradientFill {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)

    this.context.lineWidth = 2
    this.context.strokeStyle = "#FF00FF"
    wx.onTouchMove(this.touchHandler)

    let
      c1 = { x: 150, y: 150, radius:0 },
      c2 = { x: 150, y: 150, radius:50 },        
      gradient = this.context.createRadialGradient(c1.x, c1.y, c1.radius, 
                                                   c2.x, c2.y, c2.radius)

    gradient.addColorStop(0, "#FFFFFF")
    gradient.addColorStop(0.5, "#0000FF")
    gradient.addColorStop(1, "#FF0000")

    this.context.fillStyle = gradient
    this.context.fillRect(100, 100, 100, 100)
  }

  touchEventHandler(e) {

  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    // this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
  }
}