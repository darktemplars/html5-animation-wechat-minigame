import Ball from '../sprite/ball.js'

export default class CurveThroughPoint {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)

    this.context.lineWidth = 2
    this.context.strokeStyle = "#FF00FF"
    wx.onTouchMove(this.touchHandler)

    this.x0 = 100
    this.y0 = 200
    this.x2 = 300
    this.y2 = 200
  }

  touchEventHandler(e) {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    let x1 = e.touches[0].clientX * 2 - (this.x0 + this.x2) / 2
    let y1 = e.touches[0].clientY * 2 - (this.y0 + this.y2) / 2
    this.context.beginPath()
    this.context.moveTo(this.x0, this.y0)
    this.context.quadraticCurveTo(x1, y1, this.x2, this.y2)
    this.context.stroke()
  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    // this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
  }
}