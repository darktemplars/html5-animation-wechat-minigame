import Ball from '../sprite/ball.js'

export default class GradientFill {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)

    this.context.lineWidth = 2
    this.context.strokeStyle = "#FF00FF"
    wx.onTouchMove(this.touchHandler)

    let 
      pt1 = { x: 100, y: 100 },        // gradient start point
      pt2 = { x: 200, y: 200 },        // gradient end point
      gradient = this.context.createLinearGradient(pt1.x, pt1.y, pt2.x, pt2.y)

    // gradient from white to red
    // gradient.addColorStop(0, "#FFFFFF")
    // gradient.addColorStop(0.5, "#0000FF")
    // gradient.addColorStop(1, "#FF0000")
    gradient.addColorStop(0, "#FFFFFF")
    gradient.addColorStop(1, "rgba(255, 255, 255, 0)")

    this.context.fillStyle = gradient
    this.context.fillRect(100, 100, 100, 100)
  }

  touchEventHandler(e) {

  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    // this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
  }
}