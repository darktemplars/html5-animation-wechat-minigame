import Ball from '../sprite/ball.js'

export default class LoadImage {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)

    this.context.lineWidth = 2
    this.context.strokeStyle = "#FF00FF"
    wx.onTouchMove(this.touchHandler)

    let image = wx.createImage()
    image.src = "res/picture.jpg"
    image.onload = () => {
      this.context.drawImage(image, 0, 0)
    }
    image.onerror = (e) => {
      console.log(e)
    }
  }
  //http://27.221.31.207/657262189143E8363FD44C55CD/030020010058836F849F7400000001813C8487-A8A6-931A-5853-2C52E557AC00.mp4?ccode=050F&amp;duration=34&amp;expire=18000&amp;psid=1c9f80903aab82dd420561216c06f5f5&amp;ups_client_netip=733c3a9e&amp;ups_ts=1544178206&amp;ups_userid=&amp;utid=qA0tE7mnBiYCAasPnbtJLQp8&amp;vid=XMjE5NjE0NjkzMg%3D%3D&amp;vkey=A2d328da34c03159da3cac9e8099df37c&amp;sp=

  touchEventHandler(e) {
    
  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    // this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
  }
}