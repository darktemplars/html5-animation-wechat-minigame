import Ball from '../sprite/ball.js'

export default class DrawingApp {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.touchHandler = this.touchEventHandler.bind(this)

    this.context.lineWidth = 2
    this.context.strokeStyle = "#FF00FF"
    
    wx.onTouchStart((e)=> {
      console.log("touce start", e)
      this.context.beginPath()
      this.context.moveTo(e.touches[0].clientX | 0, e.touches[0].clientY | 0)
      wx.onTouchMove(this.touchHandler)
    })

    wx.onTouchEnd((e)=> {
      console.log("touce end", e)
      wx.offTouchMove(this.touchHandler)
    })
  }

  touchEventHandler(e) {
    this.context.lineTo(e.touches[0].clientX | 0, e.touches[0].clientY | 0)
    this.context.stroke()
  }
}