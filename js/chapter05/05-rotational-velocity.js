import Arrow from '../sprite/arrow.js'

export default class RotationalVelocity {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)
    wx.onTouchMove(this.touchHandler)

    this.arrow = new Arrow()
    this.addChild(this.arrow)
    this.arrow.x = this.canvas.width / 2
    this.arrow.y = this.canvas.height / 2
    this.vr = 2
  }

  touchEventHandler(e) {
    
  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
    this.arrow.rotation += this.vr * Math.PI / 180
  }
}