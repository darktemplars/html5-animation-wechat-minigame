import Ball from '../sprite/ball.js'

export default class Velocity {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)
    wx.onTouchMove(this.touchHandler)

    this.vx = 1

    this.ball = new Ball()
    this.addChild(this.ball)
    this.ball.x = 50
    this.ball.y = 100
  }

  touchEventHandler(e) {
    // e.touches[0].clientX
  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
    this.ball.x += this.vx
  }
}