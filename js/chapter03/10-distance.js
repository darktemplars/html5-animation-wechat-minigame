import Ball from '../sprite/ball.js'

export default class Distance {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)
    wx.onTouchMove(this.touchHandler)

    this.context.lineWidth = 0.5
    this.context.strokeStyle = "#FF00FF"

    let rect1 = {
      x:Math.random() * this.canvas.width,
      y:Math.random() * this.canvas.height,
    }
    this.context.fillStyle = "#FFFFFF"
    this.context.fillRect(rect1.x - 2, rect1.y - 2, 8, 8)

    var rect2 = {
      x: Math.random() * this.canvas.width,
      y: Math.random() * this.canvas.height,
    }

    this.context.fillStyle = "#FF0000"
    this.context.fillRect(rect2.x - 2, rect2.y - 2, 8, 8)

    let dx = rect1.x - rect2.x,
        dy = rect1.y - rect2.y,
        dist = Math.sqrt(dx*dx + dy*dy)

    this.context.moveTo(rect1.x, rect1.y)
    this.context.lineTo(rect2.x, rect2.y)
    this.context.stroke()

    console.log(`distance: ${dist}`)
  }

  touchEventHandler(e) {

  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    // this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
  }
}