import Arrow from '../sprite/arrow.js'

export default class RotateToMouse {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)
    wx.onTouchMove(this.touchHandler)

    this.arrow = new Arrow()
    this.addChild(this.arrow)
    this.arrow.x = this.canvas.width / 2
    this.arrow.y = this.canvas.height / 2
  }

  touchEventHandler(e) {
    let dx = e.touches[0].clientX - this.arrow.x
    let dy = e.touches[0].clientY - this.arrow.y
    this.arrow.rotation = Math.atan2(dy, dx)
  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
  }
}