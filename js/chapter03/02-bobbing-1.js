import Ball from '../sprite/ball.js'

export default class Bobbing {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)
    wx.onTouchMove(this.touchHandler)

    this.angle = 0
    this.centerY = 200
    this.range = 50
    this.speed = 0.05

    this.ball = new Ball()
    this.addChild(this.ball)
    this.ball.x = this.canvas.width / 2
    this.ball.y = this.canvas.height / 2
  }

  touchEventHandler(e) {
   
  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
    this.ball.y = this.centerY / 2 + Math.sin(this.angle) * this.range
    this.angle += this.speed
  }
}