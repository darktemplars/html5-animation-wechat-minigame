import Ball from '../sprite/ball.js'

export default class Wave {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)
    wx.onTouchMove(this.touchHandler)

    this.angleX = 0
    this.angleY = 0
    this.centerX = this.canvas.width / 2
    this.centerY = this.canvas.height / 2
    this.range = 50
    this.xSpeed = 1
    this.ySpeed = 0.05
    this.xPos = 0
    this.yPos = this.centerY

    this.context.lineWidth = 2
    this.context.strokeStyle = "#FFFFFF"
  }

  touchEventHandler(e) {

  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    // this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
    this.context.beginPath()
    this.context.moveTo(this.xPos, this.yPos)
    this.xPos += this.xSpeed
    this.angleY += this.ySpeed
    this.yPos = this.centerY + Math.sin(this.angleY) * this.range
    this.context.lineTo(this.xPos, this.yPos)
    this.context.stroke()
  }
}