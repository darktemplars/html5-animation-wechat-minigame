import Ball from '../sprite/ball.js'

export default class Random {
  constructor() {
    this.canvas = wx.createCanvas()
    this.context = this.canvas.getContext('2d')
    this.sprites = []
    this.loop = this.drawFrame.bind(this)
    this.touchHandler = this.touchEventHandler.bind(this)
    wx.onTouchMove(this.touchHandler)

    this.angleX = 0
    this.angleY = 0
    this.centerX = this.canvas.width / 2
    this.centerY = this.canvas.height / 2
    this.range = 50
    this.xSpeed = 0.07
    this.ySpeed = 0.11

    this.ball = new Ball()
    this.addChild(this.ball)
    this.ball.x = this.canvas.width / 2
    this.ball.y = this.canvas.height / 2
  }

  touchEventHandler(e) {

  }

  addChild(sprite) {
    if (sprite) {
      this.sprites.push(sprite)
    }
  }

  drawFrame() {
    requestAnimationFrame(this.loop)
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    for (let sprite of this.sprites) {
      sprite.draw(this.context)
    }
    this.ball.x = this.centerX + Math.sin(this.angleX) * this.range
    this.ball.y = this.centerY + Math.sin(this.angleY) * this.range
    this.angleX += this.xSpeed
    this.angleY += this.ySpeed
  }
}